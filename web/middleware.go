package web

import (
	"library_management/dataBase"
	"library_management/users"
	"log"
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo"
)

func checkCookie(next echo.HandlerFunc) echo.HandlerFunc {
	//var JwtKey string
	return func(c echo.Context) error {
		cookie, err := c.Cookie("token")
		if err != nil {
			log.Println(err)
			return c.JSON(http.StatusUnauthorized, echo.Map{"error": "there is no cookie"})
		}
		///
		tokenStr := cookie.Value
		claims := &users.Claims{}
		tkn, err := jwt.ParseWithClaims(tokenStr, claims,
			func(t *jwt.Token) (interface{}, error) {
				return JwtKey, nil
			})

		if err != nil {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
		}

		var temp users.User
		temp, err = dataBase.GetUser(claims.Username)

		if err != nil {
			return c.JSON(http.StatusInternalServerError, echo.Map{"error": err.Error()})
		}

		if temp.UserName != claims.Username {
			return c.JSON(http.StatusNotFound, echo.Map{"message": "wrong cookie"})
		}

		//////

		if !tkn.Valid {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "wrong cookie"})
		}

		return next(c)
	}
}

func checkRole(next echo.HandlerFunc) echo.HandlerFunc {
	//var JwtKey string
	return func(c echo.Context) error {
		cookie, err := c.Cookie("token")
		if err != nil {
			log.Println(err)
			return c.JSON(http.StatusUnauthorized, echo.Map{"error": "there is no cookie"})
		}
		///
		tokenStr := cookie.Value
		claims := &users.Claims{}
		tkn, err := jwt.ParseWithClaims(tokenStr, claims,
			func(t *jwt.Token) (interface{}, error) {
				return JwtKey, nil
			})

		if err != nil {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
		}

		var temp users.User
		temp, err = dataBase.GetUser(claims.Username)

		if err != nil {
			return c.JSON(http.StatusInternalServerError, echo.Map{"error": err.Error()})
		}

		if temp.UserName != claims.Username {
			return c.JSON(http.StatusNotFound, echo.Map{"message": "wrong cookie"})
		}

		if temp.Role != "bookkeeper" {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "you dont have premission"})
		}
		//////

		if !tkn.Valid {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "wrong cookie"})
		}

		return next(c)
	}
}
