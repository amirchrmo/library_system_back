package web

import (
	"library_management/loadConfigs"
)

var (
	host string = loadConfigs.ServerConfigs.Host
	Port string = loadConfigs.ServerConfigs.Port
)
var JwtKey = []byte("mySecret")

var configuration = host + ":" + Port
