package web

import (
	"crypto/tls"
	"fmt"
	"library_management/dataBase"
	"library_management/time"
	"net/http"

	"github.com/labstack/echo"
	"gopkg.in/gomail.v2"
)

var e = echo.New()

func RunServer() {

	e.GET("/api/v1/h", hello)
	e.File("/api/v1/", "dist/index.html")
	e.POST("/api/v1/user/login", Login)
	e.GET("/api/v1/user/logout", logout, checkCookie)
	e.POST("/api/v1/user/searchbook", searchBooks, checkCookie)
	e.POST("/api/v1/user/loan", LoanBook, checkCookie, checkRole)
	e.POST("/api/v1/user/takeback", TakeBack, checkCookie, checkRole)
	e.POST("/api/v1/user/access", AccessMemberPro, checkCookie)
	e.POST("api/v1/user/register", Registeration)
	e.POST("api/v1/user/alluser", AccessAll, checkCookie, checkRole)
	e.GET("api/v1/user/send/email", SendEmail, checkCookie, checkRole)
	e.Logger.Fatal(e.Start(configuration))
}

func hello(c echo.Context) error {
	return c.JSON(http.StatusOK, echo.Map{"message": "hello every one"})
}

type pagination struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

func AccessAll(c echo.Context) error {
	temp := pagination{}
	err := c.Bind(&temp)

	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}

	result, count, err2 := dataBase.AllUsers(temp.Page, temp.Limit)

	if err2 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err2.Error()})
	}

	for i := range result {
		z, _ := dataBase.SearchBookCurrentOwner(result[i].UserName)
		result[i].Books = z
	}

	return c.JSON(http.StatusAccepted, echo.Map{"data": result, "totalCount": count})
}

func SendEmail(c echo.Context) error {

	books, err := dataBase.AllBooks()
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}

	var check bool
	for _, j := range books {
		if j.Status == "notLoaned" {
			continue
		}
		check = time.CheckDelay(j.EndDate)

		err = dataBase.UpdateDelay(j.Id, check)
		if err != nil {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
		}

		if check {
			temp, errn := dataBase.GetUser(j.CurrentOwner)
			if errn != nil {
				return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
			}
			err = Email(j.Name, temp.Email)

			if err != nil {
				return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
			}
		}

	}

	return c.JSON(http.StatusOK, echo.Map{"message": "the operation finished"})
}

func Email(name string, Receiveremail string) error {
	m := gomail.NewMessage()

	fmt.Println(Receiveremail)
	// Set E-Mail sender
	m.SetHeader("From", "navidkaboudiniari@gmail.com")

	// Set E-Mail receivers
	m.SetHeader("To", Receiveremail)

	// Set E-Mail subject
	m.SetHeader("Subject", "library")

	// Set E-Mail body. You can set plain text or html with text/html
	m.SetBody("text/plain", "return this book "+name+"to library")

	// Settings for SMTP server
	d := gomail.NewDialer("smtp.gmail.com", 587, "navidkaboudiniari@gmail.com", "navid1378m")

	// This is only needed when SSL/TLS certificate is not valid on server.
	// In production this should be set to false.
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Now send E-Mail
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)
		return err
	}

	return nil

}
