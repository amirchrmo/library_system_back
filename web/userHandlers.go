package web

import (
	"fmt"
	"library_management/dataBase"
	"library_management/mybooks"
	"library_management/users"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type UserLogin struct {
	Username string `bson:"userName" json:"username"`
	Password string `bson:"userName" json:"password"`
}

func Login(c echo.Context) error {

	temp := UserLogin{}
	errb := c.Bind(&temp)

	if errb != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": errb.Error()})
	}

	result, err := dataBase.GetUser(temp.Username)

	//c.Response().Header().Set("Content-Length", "10000")
	// a := c.Request().Header

	// fmt.Print("length:")
	// fmt.Print(a)
	if err != nil {

		return c.JSON(http.StatusNotFound, echo.Map{"error": err.Error()})
	}
	checkPassword := dataBase.CheckPasswordHash(temp.Password, result.Password)
	if !checkPassword {
		return c.JSON(http.StatusUnauthorized, echo.Map{"message": "password is wrong"})
	}
	claims := users.Claims{
		Username:       result.UserName,
		Role:           result.Role,
		StandardClaims: jwt.StandardClaims{},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, tknerr := token.SignedString(JwtKey)

	if tknerr != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}

	cookie := &http.Cookie{}
	cookie.Name = "token"
	cookie.Value = tokenString
	cookie.Expires = time.Now().Add(48 * time.Hour)
	c.SetCookie(cookie)

	return c.JSON(http.StatusOK, echo.Map{"message": "welcome", "user": result.Rest(), "token": tokenString})
}

func logout(c echo.Context) error {
	//cookie := c.Cookie("token")
	cookie, err := c.Cookie("token")
	if err != nil {
		c.JSON(http.StatusInternalServerError, echo.Map{"error": err.Error()})
	}
	cookie.MaxAge = -1
	c.SetCookie(cookie)
	//fmt.Println(cookie.Expires)
	return c.JSON(http.StatusOK, echo.Map{"message": "you are loged out "})
}

func searchBooks(c echo.Context) error {

	temp := mybooks.Book{}
	result := []mybooks.Book{}
	err := c.Bind(&temp)

	if err != nil {

		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err})
	}
	var total int
	result, total, err = dataBase.SearcBook(&temp)
	//fmt.Println(result)
	return c.JSON(http.StatusOK, echo.Map{"data": result, "totalCountOfData": total})
}

func Registeration(c echo.Context) error {
	temp := users.User{}
	err := c.Bind(&temp)
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}
	temp.Role = "member"
	temp.Id = bson.NewObjectId()
	fmt.Println(temp)
	err = dataBase.AddUser(&temp)

	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}

	return c.JSON(http.StatusAccepted, echo.Map{"message": "new user registered"})

}
