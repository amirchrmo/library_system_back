package web

import (
	"encoding/hex"
	"fmt"
	"library_management/dataBase"
	"library_management/mybooks"
	"net/http"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type DELBook struct {
	Username string `json:"username"`
	Bsonid   string `json:"bsonid"`
}

type TakeBackBoock struct {
	Bsonid string `json:"bsonid"`
}

type Member struct {
	Username string `json:"username"`
}

type ObjectId string

func LoanBook(c echo.Context) error {

	tempnew := DELBook{}
	errb := c.Bind(&tempnew)

	if errb != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": errb.Error()})
	}

	var err error

	x, usrerr := dataBase.GetUser(tempnew.Username)

	// data base error
	if usrerr != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "user not found"})
	}
	//checking the user existing
	if x.UserName == "" {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "user not found"})
	}

	//binding error
	// if err != nil {
	// return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err})
	// }

	listOfLoanedBooks, err4 := dataBase.SearchBookCurrentOwner(x.UserName)

	// data base error
	if err4 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err4.Error()})
	}
	if len(listOfLoanedBooks) >= 3 {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "you are alredy loaned 3 books and we cant loan you another book"})
	}
	////////////////////////////////////////////////////

	s, err4_5 := ObjectIdHex(tempnew.Bsonid)
	fmt.Println(s)
	if err4_5 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "wrong format for id"})
	}

	result, err5 := dataBase.SearchBookForLoan(bson.ObjectIdHex(tempnew.Bsonid))

	//data base error
	if err5 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "the book with enterd id doesnt exist"})
	}
	// check book existing
	if result.Name == "" {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "the book with enterd id doesnt exist"})
	}
	fmt.Println(listOfLoanedBooks)

	if result.CurrentOwner == x.UserName {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "currently you have the book"})
	}

	if result.Status == "loaned" {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "the book is loaned and you can not have it"})
	}

	for _, j := range listOfLoanedBooks {
		if j.BookId == result.BookId {
			return c.JSON(http.StatusNotAcceptable, echo.Map{"message": "شما در حال حاضر یک نسخه از این کتاب به امانت دارید"})
		}
	}

	err = dataBase.UpdateBookForLoan(bson.ObjectIdHex(tempnew.Bsonid), "loaned", tempnew.Username)
	// data base error
	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "the Loaned operation has blocked"})
	}

	return c.JSON(http.StatusOK, echo.Map{"message": "book is loaned and delivered to member"})
}

func ObjectIdHex(s string) (ObjectId, error) {
	d, err := hex.DecodeString(s)
	if err != nil || len(d) != 12 {
		return "", err
	}
	return ObjectId(d), err
}

func TakeBack(c echo.Context) error {
	temp := TakeBackBoock{}
	err := c.Bind(&temp)

	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}

	s, err4_5 := ObjectIdHex(temp.Bsonid)
	fmt.Println(s)
	if err4_5 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "wrong format for id"})
	}
	result, err5 := dataBase.SearchBookForLoan(bson.ObjectIdHex(temp.Bsonid))
	//data base error
	if err5 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "the book with enterd id doesnt exist"})
	}

	if result.Status == "notLoaned" && result.CurrentOwner == "" {
		return c.JSON(http.StatusOK, echo.Map{"message": "the book alredy exist in library"})
	}

	zx := mybooks.Duration{}
	result.CurrentOwner = ""
	result.Delay = false
	result.Status = "notLoaned"
	result.StartDate = zx
	result.EndDate = zx

	err6 := dataBase.UpdateTakeBack(&result)

	if err6 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err6.Error()})
	}

	return c.JSON(http.StatusOK, echo.Map{"message": "take backing book operation is successfully done"})
}

func AccessMemberPro(c echo.Context) error {
	temp := Member{}
	err := c.Bind(&temp)

	if err != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": err.Error()})
	}

	UserInfo, err2 := dataBase.GetUser(temp.Username)

	if err2 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": "cant find user"})
	}

	BookList, ererr3 := dataBase.SearchBookCurrentOwner(temp.Username)

	if ererr3 != nil {
		return c.JSON(http.StatusNotAcceptable, echo.Map{"error": ererr3.Error()})
	}

	return c.JSON(http.StatusOK, echo.Map{"User": UserInfo, "books": BookList})

}
