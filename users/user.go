package users

import (
	"library_management/mybooks"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id       bson.ObjectId `bson:"_id"`
	UserName string        `bson:"userName" json:"userName"`
	Password string        `bson:"password" json:"password"`
	Role     string        `bson:"role" json:"role"`
	FullName string        `bson:"fullName" json:"fullName"`
	Email    string        `bson:"email" json:"email"`
}

type Claims struct {
	Username string
	Role     string
	jwt.StandardClaims
}

func (v User) Rest() echo.Map {
	return echo.Map{
		"id":       v.Id,
		"username": v.UserName,
		"fullname": v.FullName,
		"password": v.Password,
		"role":     v.Role,
		"email":    v.Email,
	}
}

type ACUser struct {
	Id       bson.ObjectId  `bson:"_id"`
	UserName string         `bson:"userName" json:"userName"`
	Password string         `bson:"password" json:"password"`
	Role     string         `bson:"role" json:"role"`
	FullName string         `bson:"fullName" json:"fullName"`
	Email    string         `bson:"email" json:"email"`
	Books    []mybooks.Book `json:"books"`
}
