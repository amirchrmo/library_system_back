package loadConfigs

import (
	"encoding/json"
	"os"
)

type Configs struct {
	DatabaseCnf struct {
		DbName string `json:"dbname"`
		Url    string `json:"url"`
	} `json:"database"`
	ServerCnf struct {
		Host string `json:"host"`
		Port string `port:"8080"`
	} `json:"server_config"`
}

func loadConfiguration() (config Configs, err error) {
	//var config Configs
	var configFile *os.File
	configFile, err = os.Open("config.json")

	if err != nil {
		return config, err
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		return config, err
	}
	configFile.Close()
	return config, nil
}

var allConfigs, ConfErr = loadConfiguration()
var DbConfigs = allConfigs.DatabaseCnf
var ServerConfigs = allConfigs.ServerCnf
