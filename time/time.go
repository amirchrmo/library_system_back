package time

import (
	"library_management/mybooks"

	ptime "github.com/yaa110/go-persian-calendar"
)

func CheckDelay(end mybooks.Duration) bool {

	Nowpt := ptime.Now()

	Nmounth := validateMounth(Nowpt.Month().String())
	Emounth := validateMounth(end.Mounth)

	if Nowpt.Year() > end.Year {
		return true
	}
	if Nmounth > Emounth {
		return true
	}
	if Nowpt.Day() > end.Day {
		return true
	}
	return false

}

func validateMounth(s string) int {
	var result int
	switch s {
	case "فروردین":
		result = 1
	case "اردیبهشت":
		result = 2
	case "خرداد":
		result = 3
	case "تیر":
		result = 4
	case "مرداد":
		result = 5
	case "شهریور":
		result = 6
	case "مهر":
		result = 7
	case "آبان":
		result = 8
	case "آذر":
		result = 9
	case "دی":
		result = 10
	case "بهمن":
		result = 11
	case "اسفند":
		result = 12
	}
	return result
}
