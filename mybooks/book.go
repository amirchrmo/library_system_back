package mybooks

import "gopkg.in/mgo.v2/bson"

type Book struct {
	Id           bson.ObjectId `bson:"_id"`
	Category     string        `bson:"category" json:"category"`
	Name         string        `bson:"name" json:"name"`
	BookId       string        `bson:"bookId" json:"bookId"`
	Writer       string        `bson:"writer" json:"writer"`
	Publisher    string        `bson:"publisher" json:"publisher"`
	Status       string        `bson:"status" json:"status"`
	CurrentOwner string        `bson:"currentOwner" json:"currentOwner"`
	Delay        bool          `bson:"delay" json:"delay"`
	Page         int           `json:"page"`
	Limit        int           `json:"limit"`
	StartDate    Duration      `bson:"startday" json:"startday"`
	EndDate      Duration      `bson:"enddate" json:"enddate"`
}

type Duration struct {
	Year   int    `bson:"year" json:"year"`
	Mounth string `bson:"mounth" json:"mounth"`
	Day    int    `bson:"day" json:"day"`
}

type ACBook struct {
	Id           bson.ObjectId `bson:"_id"`
	Category     string        `bson:"category" json:"category"`
	Name         string        `bson:"name" json:"name"`
	BookId       string        `bson:"bookId" json:"bookId"`
	Writer       string        `bson:"writer" json:"writer"`
	Publisher    string        `bson:"publisher" json:"publisher"`
	Status       string        `bson:"status" json:"status"`
	CurrentOwner string        `bson:"currentOwner" json:"currentOwner"`
	Delay        bool          `bson:"delay" json:"delay"`
	StartDate    Duration      `bson:"startday" json:"startday"`
	EndDate      Duration      `bson:"enddate" json:"enddate"`
}
