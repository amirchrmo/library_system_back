package dataBase

import (
	"library_management/loadConfigs"

	"gopkg.in/mgo.v2"
)

var (
	dbName   = loadConfigs.DbConfigs.DbName
	url      = loadConfigs.DbConfigs.Url
	session  *mgo.Session
	UserColl *mgo.Collection
	BookColl *mgo.Collection
	err      error
)

func Start() {
	session, err = mgo.Dial(url)
	if err != nil {
		panic(err)
	}

	// we gonna compelet this part
	UserColl = session.DB(dbName).C("users")
	BookColl = session.DB(dbName).C("book")
	index := mgo.Index{
		Key:    []string{"userName"},
		Unique: true,
	}

	err = UserColl.EnsureIndex(index)
	if err != nil {
		panic(err)
	}

}

func End() {
	session.Close()
}
