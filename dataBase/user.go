package dataBase

import (
	"library_management/users"

	"gopkg.in/mgo.v2/bson"
)

func AddUser(temp *users.User) error {
	var hashErr error
	temp.Password, hashErr = HashPassword(temp.Password)

	if hashErr != nil {
		return hashErr
	}

	if err != nil {
		return err
	}
	err = UserColl.Insert(temp)

	return err
}
func GetUser(username string) (users.User, error) {

	temp := users.User{}
	var err error
	err = UserColl.Find(bson.M{"userName": username}).One(&temp)
	// or
	//err := Coll.Find(bson.M{"username": bson.M{"$eq": userName}}).One(&result)
	return temp, err
}

func AllUsers(page, limit int) ([]users.ACUser, int, error) {
	temp := []users.ACUser{}

	a, err2 := UserColl.Find(nil).Count()
	if err2 != nil {
		return nil, 0, err2
	}

	err := UserColl.Find(nil).Skip((page - 1) * limit).Limit(limit).All(&temp)

	return temp, a, err
}
