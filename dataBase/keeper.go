package dataBase

import (
	"library_management/mybooks"

	ptime "github.com/yaa110/go-persian-calendar"
	"gopkg.in/mgo.v2/bson"
)

func UpdateBookForLoan(bookId bson.ObjectId, status string, currentOwner string) error {
	colQuerier := bson.M{"_id": bookId}
	pt := ptime.Now()
	temp := mybooks.Duration{}
	temp.Year = pt.Year()
	temp.Mounth = pt.Month().String()
	temp.Day = pt.Day()

	pt2 := pt.AddDate(0, 0, 14)
	temp2 := mybooks.Duration{}
	temp2.Year = pt2.Year()
	temp2.Mounth = pt2.Month().String()
	temp2.Day = pt2.Day()

	change := bson.M{"$set": bson.M{"status": status, "currentOwner": currentOwner, "startdate": temp, "enddate": temp2}}

	err := BookColl.Update(colQuerier, change)
	if err != nil {
		return err
	}
	return nil
}

func UpdateTakeBack(a *mybooks.Book) error {
	colQuerier := bson.M{"_id": a.Id}
	change := bson.M{"$set": bson.M{"status": a.Status, "currentOwner": a.CurrentOwner, "delay": a.Delay, "startdate": a.StartDate, "enddate": a.EndDate}}

	err := BookColl.Update(colQuerier, change)
	if err != nil {
		return err
	}
	return nil
}
