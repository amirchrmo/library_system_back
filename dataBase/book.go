package dataBase

import (
	"library_management/mybooks"

	"gopkg.in/mgo.v2/bson"
)

func AddBooks(temp *mybooks.Book) error {

	err = BookColl.Insert(temp)
	return err
}

func SearcBook(temp *mybooks.Book) ([]mybooks.Book, int, error) {

	var totalCount int

	result := []mybooks.Book{}

	queryElements := []bson.M{}
	if temp.Name != "" {
		queryElements = append(queryElements, bson.M{"name": temp.Name})
	}
	if temp.Category != "" {
		queryElements = append(queryElements, bson.M{"category": temp.Category})
	}
	if temp.Writer != "" {
		queryElements = append(queryElements, bson.M{"writer": temp.Writer})
	}
	if temp.Publisher != "" {
		queryElements = append(queryElements, bson.M{"publisher": temp.Publisher})
	}
	if temp.Status != "" {
		queryElements = append(queryElements, bson.M{"status": temp.Status})
	}

	skipParametr := (temp.Page - 1) * temp.Limit

	if len(queryElements) == 0 {

		err = BookColl.Find(nil).Skip(skipParametr).Limit(temp.Limit).All(&result)
		return result, 535, err
	}

	query := bson.M{"$and": queryElements}

	totalCount, _ = BookColl.Find(query).Count()
	err = BookColl.Find(query).Skip(skipParametr).Limit(temp.Limit).All(&result)

	return result, totalCount, err
}

func SearchBookForLoan(id bson.ObjectId) (mybooks.Book, error) {
	result := mybooks.Book{}

	err := BookColl.Find(bson.M{"_id": id}).One(&result)

	return result, err
}

func SearchBookCurrentOwner(userName string) ([]mybooks.Book, error) {

	result := []mybooks.Book{}
	err := BookColl.Find(bson.M{"currentOwner": userName}).All(&result)
	return result, err

}

func AllBooks() ([]mybooks.Book, error) {
	result := []mybooks.Book{}
	err := BookColl.Find(nil).All(&result)
	return result, err
}

func UpdateDelay(id bson.ObjectId, d bool) error {
	colQuerier := bson.M{"_id": id}
	change := bson.M{"$set": bson.M{"delay": d}}
	err := BookColl.Update(colQuerier, change)
	if err != nil {
		return err
	}
	return nil
}
