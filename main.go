package main

import (
	"fmt"
	"library_management/dataBase"
	"library_management/mybooks"
	"library_management/web"

	"github.com/xuri/excelize/v2"
	ptime "github.com/yaa110/go-persian-calendar"
	"gopkg.in/mgo.v2/bson"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	dataBase.Start()
	// z := "61c0a09df54eb68b985914ad"
	// a, _ := dataBase.SearchBookForLoan(bson.ObjectIdHex(z))
	// fmt.Println(a)

	// temp := mybooks.Book{}
	// temp.Name = "سال بلوا"
	// temp.Status = "notLoaned"
	// x, _ := dataBase.SearcBook(&temp)
	// fmt.Println(x)

	web.RunServer()
	//err := dataBase.UpdateBook("ال بلوا", "loaned", "navid1378")
	//panic(err)
	//write()
	// temp, _ := mybooks.Parse()
	// write(temp)

	// timeeeee()

}

func timeeeee() {
	pt := ptime.Now()
	//t := ptime.Date(1394, ptime.Mehr, 2)
	//fmt.Println(t)
	fmt.Println(pt.Date())
	pt.AddDate(0, 0, -3).Date()
	fmt.Println(pt.Date())
}

func Parse() (string, error) {
	// loading books data set from book.xlsx to mongo Book collection in library data base
	var temp string
	f, err := excelize.OpenFile("book.xlsx")
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	// Get value from cell by given worksheet name and axis.
	cell, err := f.GetCellValue("Sheet1", "B2")
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	fmt.Println(cell)
	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("Sheet1")
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	// for _, row := range rows {
	// for _, colCell := range row {
	// fmt.Print(colCell, "\t")
	// }
	// fmt.Println()
	// }

	temp2 := mybooks.Book{}
	for i, j := range rows {
		for z, k := range j {
			if i == 0 {
				continue
			}
			if z == 0 {
			} else if z == 1 {
				temp2.Name = k
			} else if z == 2 {
				temp2.Writer = k
			} else if z == 3 {
				temp2.Publisher = k
			} else if z == 4 {
				temp2.Category = k
			}
			temp2.Id = bson.NewObjectId()
			temp2.CurrentOwner = ""
			temp2.Delay = false
			temp2.Status = "notLoaned"
		}
		dataBase.AddBooks(&temp2)
	}

	return temp, nil
}
